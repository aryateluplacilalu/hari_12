<?php
class About extends Controller
{
    public function index($namadepan = 'Luhur', $namabelakang = 'Budi Aryanto')
    {
        $data[$namadepan] = $namadepan;
        $data[$namabelakang] = $namabelakang;
        $data['judul'] = 'Register';
        $this->view('about/form', $data);
        $this->view('templates/header', $data);
        $this->view('templates/footer');
    }
    public function page()
    {
        $data['judul'] = 'Welcome';
        $this->view('about/welcome');
        $this->view('templates/header', $data);
        $this->view('templates/footer');
    }
}
